# Magic Ball

## Descripción:
La Magic 8-Ball (en español Bola 8 Mágica) es un juguete usado para echar la fortuna o buscar consejo diseñado por Mattel.

La Magic 8-Ball es una esfera de plástico con la misma apariencia que una [bola 8](https://es.wikipedia.org/wiki/Bola_de_billar) de pool. En el interior se encuentra un depósito cilíndrico que contiene un [dado](https://es.wikipedia.org/wiki/Dado) blanco de plástico con forma [icosaédrica](https://es.wikipedia.org/wiki/Icosaedro) flotando en una tinta de color azul oscuro disuelta con alcohol. El dado está hueco y presenta aperturas en cada cara, para que el fluido entre dentro de él, esto le proporciona la mínima flotabilidad. Cada una de las 20 caras del dado tiene impresa con letras en relieve una frase afirmativa, negativa o neutra. La bola 8 tiene una “ventana” transparente en la parte superior a través de la cual se puede leer dichos mensajes.

Para usar la bola lo primero que hay que hacer es colocarla boca abajo. Después de "preguntar a la bola" una pregunta que se pueda responder con un simple sí o no, el usuario debe girar la bola colocando la ventana hacia arriba, así se pone en movimiento el líquido que se halla en su interior. Cuando el dado flota en la parte superior y una de sus caras está presionada contra la ventana, las letras en relieve desplazarán la tinta azul revelando un mensaje, que aparece con letras blancas sobre fondo azul. Al contrario de lo que se cree popularmente, no es necesario (ni recomendable) agitar la bola antes de girarla, ya que al hacerlo se crean burbujas de aire que distorsionan el mensaje.

Las 20 respuestas estándar en la Magic 8-Ball :

✅ En mi opinión, sí

✅ Es cierto

✅ Es decididamente así

✅ Probablemente

✅ Buen pronóstico

✅ Todo apunta a que sí

✅ Sin duda

✅ Sí

✅ Sí - definitivamente

✅ Debes confiar en ello

❔ Respuesta vaga, vuelve a intentarlo

❔ Pregunta en otro momento

❔ Será mejor que no te lo diga ahora

❔ No puedo predecirlo ahora

❔ Concéntrate y vuelve a preguntar

❌ No cuentes con ello

❌ Mi respuesta es no

❌ Mis fuentes me dicen que no

❌ Las perspectivas no son buenas

❌ Muy dudoso

10 de las respuestas posibles son afirmativas (✅)

5 dudosas (❔).

5 son negativas (❌)

# Manos a la obra:

Para desarrollar esta aplicación necesitaremos un par de componentes visuales:

- Botón (para preguntar)
- Etiquetas de texto (para visualizar los resultados)

Además necesitaremos otros tipos de componentes no visuales:

- Reconocimiento de voz
- Acelerómetro
- Notificador
- Texto a voz

## Codificando la idea:

Vamos a descomponer la idea de esta forma:

Tal como el funcionamiento de la Magic Ball, necesitamos enviarle una pregunta y nuestro programa nos devolverá un resultado al azar de una lista de respuestas.

Siendo así, crearemos en primera instancia la lista de respuestas disponibles (ver más arriba).

![](Web-Version/img/Lista.png)

Luego, para la creación de preguntas crearemos una función (procedimiento) el cual va a contener la acción de preguntar al sistema. Recordemos que una función o procedimiento se utiliza para no repetir el mismo código varias veces, ya que de esta forma podemos llamar a la función para ejecutar un bloque de código en varios lugares.

![](Web-Version/img/FuncionPregunta.png)

A su vez, también crearemos la función de respuesta el cual va a tomar literalmente un elemento al azar de nuestra lista de respuestas y lo mostrará en pantalla.
Además, incluimos el componente Texto a Voz para reproducir la respuesta de forma sonora.

![](Web-Version/img/Respuesta.png)

La lógica de nuestro programa ya está lista. Ahora nos queda conectarlo con los componentes requeridos para interactuar sobre esta. Utilizaremos cualquiera de las alternativas posibles para ejecutar esta función, ya sea desde el reconocimiento de voz, agitando el celular o presionando un botón

![](Web-Version/img/Pregunta.png)

![](Web-Version/img/Acelerometro.png)

![](Web-Version/img/Boton.png)

![](Web-Version/img/Notificador.png)

# Diseño de la aplicación

En este punto podemos aplicar el diseño que más nos guste ya que esto no influye en el código de nuestro programa.
