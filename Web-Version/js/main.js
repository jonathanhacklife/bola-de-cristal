﻿const respuestas = [
  "✅ En mi opinión, sí",
  "✅ Es cierto",
  "✅ Es decididamente así",
  "✅ Probablemente",
  "✅ Buen pronóstico",
  "✅ Todo apunta a que sí",
  "✅ Sin duda",
  "✅ Sí",
  "✅ Sí, definitivamente",
  "✅ Debes confiar en ello",
  "❔ Respuesta vaga, vuelve a intentarlo",
  "❔ Pregunta en otro momento",
  "❔ Será mejor que no te lo diga ahora",
  "❔ No puedo predecirlo ahora",
  "❔ Concéntrate y vuelve a preguntar",
  "❌ No cuentes con ello",
  "❌ Mi respuesta es no",
  "❌ Mis fuentes me dicen que no",
  "❌ Las perspectivas no son buenas",
  "❌ Muy dudoso"
]

function preguntar(pregunta) {
  if (pregunta != "Cancelar" && pregunta != "") {
    document.getElementById("pregunta").innerHTML = "¿" + pregunta.toUpperCase().replace(/([¿?])/g, "") + "?";
    responder();
  }
}

function responder() {
  let emitter = document.getElementsByClassName("sonar-emitter")[0];
  let wave = document.getElementsByClassName("sonar-wave")[0];

  let respuestaAlAzar = respuestas[Math.floor(Math.random() * respuestas.length)];
  document.getElementById("respuesta").innerHTML = respuestaAlAzar;
  if (respuestaAlAzar.includes("✅")) {
    emitter.style.backgroundColor = "green";
    wave.style.backgroundColor = "green";
  }
  else if (respuestaAlAzar.includes("❔")) {
    emitter.style.backgroundColor = "white";
    wave.style.backgroundColor = "white";
  }
  else {
    emitter.style.backgroundColor = "red";
    wave.style.backgroundColor = "red";
  }
  return respuestaAlAzar;
}

document.getElementById("boton").addEventListener('click', () => preguntar(prompt('¿Que desea saber?')))